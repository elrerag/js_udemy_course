let nombres = ["hugo", "paco", "luis"];

/*
* Para recorrer una lista necesitamos 3 cosas
*
* 1 - índice
* 2 - Límite (condición de salida)
* 3 - Aumentar este índice
*
* */


for(let i=0; i<nombres.length; i++){
    msj = "El nombre en la posición: " + i + " es igual a: " + nombres[i];
    console.log(msj);
}
