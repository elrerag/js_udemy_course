let nombres = ["hugo", "paco", "luis"];

/*
* Para recorrer una lista necesitamos 3 cosas
*
* 1 - índice
* 2 - Aumentar este índice
* 3 - Límite (condición de salida)
*
* */

let i = 0; // acá tenemos el índice.
let msj = "";


while(i < nombres.length){
    msj = "El nombre en la posición: " + i + " es igual a: " + nombres[i];
    console.log(msj);
    i++;
}